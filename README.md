# Project Management System

## Description
Creation of a comprehensive project management system designed for IT companies, which allows users to create and manage projects, tasks, teams and resources. The system must provide integration with external systems such as Git repositories and CI/CD tools, as well as support microservice architecture, security, testing and cloud deployment.

## Functional requirements
1. User and role management:
    * User authentication and authorization.
    * Assigning roles and permissions.
2. Project management:
    * Create, edit, view and delete projects.
    * Assigning users to projects and tasks.
3. Task management:
    * Create, edit, view and delete tasks within projects.
    * Monitor task status and priority.
4. Git integration:
    * Displays a list of commits for projects from linked Git repositories.
    * Ability to link commits to tasks.
5. Version control and CI/CD:
    * Integration with Jenkins to track the status of builds and deployments.
6. Reports and analytics:
    * Generating reports on work done and project status.
    * Dashboard with team performance metrics.
7. Microservice architecture:
    * Dividing the system into separate microservices, for example, user service, project service, analytics service.
8. Safety:
    * Implementation of protection mechanisms against XSS, CSRF and SQL injections.
    * Configuring HTTPS and HTTP Secure Headers.
9. Testing:
    * Development of unit tests for each microservice.
    * Integration testing of interservice interaction.
10. Documentation:
    * Creation of OpenAPI specification for microservices.

## Non-Technical Requirements
1. Interface:
    * Intuitive and responsive design.
    * Adaptability for various devices.
2. Availability:
    * Multilingual interface, supporting at least English and Russian languages.
3. Training and Support:
    * User's Guide for working with the system.
    * FAQ and support system for user feedback.

## Technologies
* Backend: Spring Boot, Spring Security, Spring Data JPA, Spring Cloud
* Frontend: Vue.js
* Database: PostgreSQL and MongoDB (for logging and analytics)
* Messaging: RabbitMQ
* Caching: Redis
* Deployment: Docker, Kubernetes, and use of a cloud platform (AWS, GCP or Azure)
* CI/CD tools: Jenkins, GitLab CI/CD

## Implementation Plan
* Week 1-2: Spring Boot basics, creating the basic framework of the application.
* Week 3-4: Development of core functions and microservices, security.
* Week 5-6: Integration with external services, testing.
* Week 7-8: Deployment, CI/CD, documentation and final development.

## Acceptance Criteria
* Functionality: All requirements must be implemented and work without errors.
* Performance: The system must support parallel operation of at least 1000 users.
* Security: No vulnerabilities based on automated security tests.
* Testing: Code coverage with tests is at least 80%.